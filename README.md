# Xsuite on HTCondor
[![pipeline status](https://gitlab.cern.ch/abt-optics-and-code-repository/simulation-codes/xsuite-docker/badges/master/pipeline.svg)](https://gitlab.cern.ch/abt-optics-and-code-repository/simulation-codes/xsuite-docker/-/commits/master)

This repo contains Dockerfiles and environment configurations for creating a Docker image to run [Xsuite](https://github.com/xsuite) simulations on [HTCondor](https://batchdocs.web.cern.ch/index.html), mostly at CERN. It also contains a template simulation file.

The [latest image on the master branch](https://gitlab.cern.ch/abt-optics-and-code-repository/simulation-codes/xsuite-docker/-/commits/master) built is always available at `gitlab-registry.cern.ch/abt-optics-and-code-repository/simulation-codes/xsuite-docker:latest` and updated at every commit

To create your own you may use branch the master and your branch will need to be triggered manually by going to **deploy -> Run** on the pipeline corresponding to your branch. This will trigger the creation of the image, which takes around 0.5h.

## The Docker Image

The Docker image is based on a [Micromamba image](https://hub.docker.com/r/mambaorg/micromamba), which itself is based on:
- Ubuntu 20.04 Focal Fossa
- Nvidia CUDA Toolkit 11.7.1

Mamba is a drop-in replacement for Conda (replace all `conda` commands with `mamba`). Micromamba is a lightweight version of this.

`apt-get` is used to install:
- git
- gcc compiler
- sudo

A mamba environment (`base`) is created, installing (from conda-forge):
- Python 3.11.7
- cudatoolkit
- cupy
- gxx
- cmake

The following packages are then installed (from PyPi):
- Xsuite (Xtrack, Xobjects, Xfields, Xpart, Xdeps)
- Numpy, Scipy, Pandas, Matplotlib, Holoviews, Seaborn, Selenium, Sympy, Zipp, Torch
- CPyMAD for importing MAD-X
- MadTools for lattice interactive visualization
- tqdm for timing
- pytables and fastparquet for HDF files
- [PyBT](https://gitlab.cern.ch/abt-optics-and-code-repository/simulation-codes/pybt), a package from CERN's ABT group with useful beam physics functions, from the CERN acc-py repo

You can use it on HTCondor with the following submission file
```
universe                = docker
docker_image            = gitlab-registry.cern.ch/abt-optics-and-code-repository/simulation-codes/xsuite-batch:latest
```

## Running A Simulation

(A specific example of running RFKO simulations is included in `/example`. This code is quite long and complicated, so a "template" is provided in `.`)

The provided job template includes:
- `job.sub`, the HTCondor submission file
- `job.sh`, the bash script executed on each job machine to initiate the simulation
- `job.py`, the Xsuite Python simulation script
- `job_args.txt`, a list of parameters to run a simulation for

### `job.sub`
`job.sub` defines the submissions to the HTCondor batch service. The first few lines specify which docker image to use (as built by this repo), and the bash script to be executed for each simulation job. A single argument `$(argument)$` is specified, but more can be named and appended.

The next lines specify the stderr/stdout file locations, and which files to transfer to each job machine. Once the script is complete, we expect it to generate a `$(argument)_output.pkl` file, which will be transferred back to wherever we ran the script. The next lines specify that we want a GPU available, and that the job is expected to take under 20 minutes to complete.

A job is then queued for each line in `job_args.txt`, and the argument is passed to `job.sh`. Please note: even if the script fails, and the output file is not created by `job.py`, HTCondor will transfer back an *empty* file with the correct name.

### `job.sh`

This short bash script does three things:
- enables micromamba, the python distribution/package manager
- activates the `base` environment
- runs the `job.py` file with the argument passed

### `job.py`

This python file is our simulation code. The example is taken from the [Xsuite docs](https://xsuite.readthedocs.io/en/latest/singlepart.html), but any code can be run. Once it is finished, it creates an output file which includes the argument in its name. This allows us to distinguish between files from different jobs, preventing a new job output from overwriting an old job.

### `job_args.txt`

This file contains a line for each simulation job. Multiple arguments can be specified, separated by spaces.

## Other Useful Things

- `condor_ssh_to_job -auto-retry 100100.24` -> SSH into job ID `100100.24` when it runs
- `condor_rm 123123` -> remove all of cluster `123123`'s jobs.
- `condor_q -analyze 555.33` -> find out why job `555.33` is still idle
- `condor_submit test.sub -batch-name this-is-a-test` -> submit `test.sub` but with a descriptive name so you can tell what is is straight from `condor_q`
- `watch condor_q` -> keep a live-updating condor_q open (useful for superlative monitors)