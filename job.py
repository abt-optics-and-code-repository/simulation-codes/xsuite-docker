import numpy as np

import xtrack as xt
import xpart as xp
import xobjects as xo

import sys
import pickle as pkl

# Example Code from https://xsuite.readthedocs.io/en/latest/singlepart.html

try:
    args = sys.argv[1:]
    argument = args[0]
except:
    argument = 1

ctx = xo.ContextCupy()

line = xt.Line(
    elements=[xt.Drift(length=2.),
              xt.Multipole(knl=[0, 1.], ksl=[0,0]),
              xt.Drift(length=1.),
              xt.Multipole(knl=[0, -1.], ksl=[0,0])],
    element_names=['drift_0', 'quad_0', 'drift_1', 'quad_1'])

line.particle_ref = xp.Particles(p0c=6500e9, #eV
                                 q0=1, mass0=xp.PROTON_MASS_EV)

line.build_tracker(_context=ctx)

n_part = 200
particles = xp.Particles(p0c=6500e9, #eV
                        q0=1, mass0=xp.PROTON_MASS_EV,
                        x=np.random.uniform(-1e-3, 1e-3, n_part),
                        px=np.random.uniform(-1e-5, 1e-5, n_part),
                        y=np.random.uniform(-2e-3, 2e-3, n_part),
                        py=np.random.uniform(-3e-5, 3e-5, n_part),
                        zeta=np.random.uniform(-1e-2, 1e-2, n_part),
                        delta=np.random.uniform(-1e-4, 1e-4, n_part),
                        _context=ctx
                        )

n_turns = 100
line.track(particles, num_turns=n_turns,
              turn_by_turn_monitor=True)

with open(f"{argument}_output.pkl", "wb") as fpkl:
    pkl.dump(line.record_last_track.x, fpkl)